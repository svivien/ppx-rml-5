
let long_op n =
  let s = ref 0 in
  while (!s + !s - !s - n) <> 0 do
    incr s;
    incr s;
    decr s;
  done;;

let p1 t n =
  for _ = 1 to t do
    long_op n
  done;;

let rec fibo n =
  if n < 1 then 1
  else fibo (n - 1) + fibo (n - 2)

[%%rml.Perf

  [@@@nbthread 2]

  let process p2 t n =
    for%par _i = 1 to t do
      [%ocaml long_op] n
    done

  let process p3 t n =
    for _i = 1 to t do
      [%ocaml long_op] n
    done

  let rec fibo n =
    if n < 1 then 1
    else fibo (n - 1) + fibo (n - 2)

  let rec process fibo_par n =
    if n < 38 then fibo n else
    let a = run (fibo_par (n - 1))
    and b = run (fibo_par (n - 2))
    in a + b
]

let time nbProcess n =
  let t1 = Unix.gettimeofday () in
  let n1 = Perf.run (Perf.p2 nbProcess n) in
  let t2 = Unix.gettimeofday () in
  let n2 = p1 nbProcess n in
  let t3 = Unix.gettimeofday () in
  let n3 = Perf.run (Perf.p3 nbProcess n) in
  let t4 = Unix.gettimeofday () in
  Printf.printf "Execution time for rml par: %fs\n" (t2 -. t1);
  Printf.printf "Execution time for rml seq: %fs\n" (t4 -. t3);
  Printf.printf "Execution time for ocaml  : %fs\n" (t3 -. t2);
  assert (n1 = n2 && n2 = n3);;

let time_fibo n =
  let t1 = Unix.gettimeofday () in
  let n1 = Perf.run (Perf.fibo_par n) in
  let t2 = Unix.gettimeofday () in
  let n2 = Perf.fibo n in
  let t3 = Unix.gettimeofday () in
  let n3 = fibo n in
  let t4 = Unix.gettimeofday () in
  Printf.printf "Computed fibonacci of %d\n" n;
  Printf.printf "Execution time for rml par: %fs\n" (t2 -. t1);
  Printf.printf "Execution time for rml seq: %fs\n" (t3 -. t2);
  Printf.printf "Execution time for ocaml  : %fs\n" (t4 -. t3);
  assert (n1 = n2 && n2 = n3);;

let () = time_fibo 42