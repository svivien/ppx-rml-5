build:
	dune build stdlib
	RML_RECOMPILE_RZI=0 dune build compiler tools interpreter toplevel

test: build
	dune runtest
	
darwin: build
	dune build test/darwin.exe
	./test/darwin.exe

compare: build
	dune build test/compare_perf.exe
	./test/compare_perf.exe

install: build
	dune install

uninstall:
	dune uninstall

clean:
	dune clean
