---
title: ReactiveML
author: Samuel \textsc{Vivien}
numbersections: True
advanced-maths: True
advanced-cs: True
theme: metropolis
header-includes:
    - \usepackage{setspace}
---
# Benchmark

## Performances

```ocaml
let fibo n = 
    if n < 1 then fibo (n - 1) + fibo (n - 2)

let process fibo_par n = 
    if n < 38 then fibo n else
        let a = run (fibo (n - 1))
        and b = run (fibo (n - 2))
        in a + b
```

Computed fibonacci of 42

\uncover<2->{Execution time for ocaml  : 2.10s}

\uncover<3->{Execution time for rml seq: 2.12s}

\uncover<4->{Execution time for rml par: 1.14s}
# Motivation

## OCaml 5.00

![4 avril 2022](images/promised.png)

# How it is done

## Domainslib.Task

```ocaml
val setup_pool : ?name:string ->
    num_additional_domains:int ->
    unit -> pool
val await : pool -> 'a promise -> 'a
val async : pool -> 'a task -> 'a promise
```

## Domainslib.Chan

```ocaml
val make_unbounded : unit -> 'a t
val send_pool : 'a t -> 'a -> bool
val recv_pool : 'a t -> 'a option
```

# Difficulties

## Events

```ocaml
signal s;;

let process p1 = emit s;;

let process p2 =
    present s
    then ()
```



## Future extensions

- Better handling of events
- Using effects
- Create a nice interface for the user to use mutexes of equivalent structures
