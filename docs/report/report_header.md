---
title: Towards a new interpreter for RML
author: Samuel \textsc{Vivien}
numbersections: True
header-includes:
    - \usepackage{tikz}
    - \usetikzlibrary{chains, decorations.pathreplacing}
abstract: ReactiveML is a reactive language based on OCaml, thus it doesn't support multithreading as OCaml doesn't. However with the upcomming OCaml 5.00, multithreading in OCaml will soon be a possible. This project thus uses the available functionnalities for multithreading in the beta compiler in order to improve the performances of OCaml multicore.
---

