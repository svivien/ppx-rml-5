
module type S =
  sig
    type t

    val lock : t -> unit
    val unlock : t -> unit

    val create : unit -> t

    val merge : t -> t -> t
end


module GlobalLock : S = 
  struct

  type t = Mutex.t

  let mutex = Mutex.create ()

  let lock = Mutex.lock

  let unlock = Mutex.unlock

  let create () = mutex

  let merge _ _ = mutex
end


module LocalLock : S = 
  struct

  module Mint = Map.Make(Int);;

  type t = Mutex.t Mint.t

  let count = ref 0

  let mutex = Mutex.create ()

  let mutexes = ref Mint.empty


  let create () =
    Mutex.lock mutex;
    let c = !count in
    incr count;
    Mutex.unlock mutex;
    Mint.singleton c (Mutex.create ())

  let lock =
    Mint.iter (fun _ -> Mutex.lock)
  
  let unlock =
    Mint.iter (fun _ -> Mutex.unlock)

  let merge = Mint.merge (fun k l1 l2 -> match l1, l2 with
      | Some s1, _ -> l1
      | None, Some s2 -> l2
      | None, None -> None)

end