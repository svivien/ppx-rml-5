
# Disclaimer

The code provided is not fully mine. The source has been taken from the project [ppx_rml](https://github.com/samsa1/ppx_rml). This project is itself based on the project ReactiveML. In this project we have only modified the interpreter, we recommend the reader to read the [report](https://github.com/samsa1/ppx_rml/blob/master/docs/report/report.md) from this project before reading this one in order to help him undestand the exemples of code given below.

The project `ppx_rml` modified ReactiveML compiler in order to allow compilation thought PPX (see the report corresponding to this project in the folder `docs` of it's repository).

The project presented here aims to present the modification done to the interpreter to take advantage of the `domainslib` library that will come with OCaml 5.00.

# Motivation and context

When writing those lines, OCaml 5.00 (also known as OCaml multicore) is due to April 04, 2022 says [OCaml's repository](https://github.com/ocaml/ocaml/milestone/40).

Even if this version has been promised for a long time, it is closer than ever. And demo versions are already available (`4.12.0+domains` and `4.12.0+domains+effects`).

As shown in the demo versions above, the two main additions that OCaml 5 will have in comparison to OCaml 4 are multithreading and algebraic effects. While multithreading is quite intuitive, it is a bit more complicated to understand what effects are.

Algebraic effects are a new version of the exception system in OCaml which is more expresive and can handle errors management in a multithreaded context without killing many threads.

In the following report, only `4.12.0+domains` has been used because the library `Ppxlib` is not available with the `4.12.0+domains+effects` version. Both compilers can be downloaded as explained on this [page](https://github.com/ocaml-multicore/multicore-opam).

To test the code, you will also need the last version (0.4.0) from the librairy `domainslib`.

ReactiveML is a reactive language based on OCaml who is simulating multi-threading. Thus the arrival of multi-threading in OCaml means that ReactiveML could benefit from it in order to improve the performances.

# How it is done

Multithreading the rml interpreter seams easy at first but data race are hiden in many places. I will first present how the global parallelisation was done, then present `Domainslib.Chan` the structure I used to remove many locks in the code and lastly present different difficulties that have been found while parallelising the code.

# Global parallelisation

The rml interpreter already had a list of all the continuations that needed to be computed. To allow parallel computation of the code, we just need to launch every continuation in this list in parallel rather than computing them one by one.

Thus whenever a new process was added to the list of the process currently running, instead the process is started on a different domain and the promise value is stored instead in the list. Afterward an instant (rml's time measure) is considered finished if every promise value in the list has been awaited and the list is now empty.

In order to control the number of threads, a pool of threads have been used. The usefull functions are (see [documentation](https://github.com/ocaml-multicore/domainslib/blob/master/lib/task.mli) for more information):

```ocaml
(* in Domainslib.Task *)
val setup_pool : ?name:string -> num_additional_domains:int -> unit -> pool
val await : pool -> 'a promise -> 'a
val async : pool -> 'a task -> 'a promise
```

When the computation is on hold between two instants the process that should be added to `current` are added to the list `to_launch` instead that will then be transfered into `current` at the start of the instant in order to prevent launching threads before they are allowed to be computed.

The other thing main thing that changed is that the `sched` function has been removed from the code. We don't need to schedule the next action to do anymore, the thread just needs to end his computation and the pool will due the scheduling without our help. Only a few calls to `sched` have been replaced by calls to `await` instead where `await` launch the process stored in `to_launch` and then waits for every thread in the pool to finish.

This parallelisation caused many data races, I will now talk about the different situation where they can be encountered and how they where handled.

The default protection against data races are mutexes. They are have been the first protection against data races that it put before replacing them with more elegant structures. But they are still used a few times explicitly inside the code.

# Domainslib.Chan

The `domainslib` library contains a usefull module called `Chan`, this corresponds to pipes.

While looking inside the original code of the interpreter we can see that references of list are used a lot. And most of the time they are accessed, the process either adds values to it or apply a function to every element of the list before emptying it. Thus we clearly understand that those list references are used as a pipe most of the time.

Thus many references of list have been replaced by channels as this structure had the advantage of being thread-safe while the following code wasn't :

```ocaml
my_list := value :: !my_list
```

# The diffuculties of events

The main difficulty that was left was the handling of events. To understand the need of mutexes we will use the following exemple :

```ocaml
let s = Signal

let process f1 =
    for i = 1 to 100 do
        emit s;
        pause;
    done

let process f2 = 
    let n = ref 0 in
    for i = 1 to 100 do
        if%present s
        then
            begin
                incr n;
                pause;
            end
    done;
    print_int !n;
    print_newline ();
    
let () = run f1 || run f2
```

If you really are unlucky the code above could print a result smaller than 100 is event are not secured.

A problem arises if in an instant :

- process `f2` tests if the signal `s` is present and it is not,
- process `f1` sends the signal `s` and wakes every process waiting for the signal `s`,
- process `f2` register itself in the list of the process that needs to be woke up when the signal `s` is emitted.

To prevent this from happening the handling of event needs to be protect. To enable this protection, the module related to events in defined in `interpreter/sig_env.ml` have been replace with functors that take an `Event_security` as argument. Two different `Event_security` have been implemented (see corresponding file).

- GlobalLock : all signals share one common mutex.
- LocalLock : every signal has it's own mutex and an order is defined on the mutexes in order to prevent deadlocks.

# Benchmark

When doing `dune build test/compare_perf.exe && ./test/compare_perf.exe`

You get the following result :

*Computed fibonacci of 42*

*Execution time for rml par: 1.532470s*

*Execution time for rml seq: 2.835766s*

*Execution time for ocaml  : 2.896087s*

Result obtained on a dual-core CPU. You can modify the file `test/compare_perf.ml` in order to change the argument of the function `Perf.set_num_threads` to adapt it to the number of cores you got in order to make a better comparison.

This result clearly shows the benefits from parallelising the RML interpreter.

# Possible improvements

- Try to reduce the numbers of locks in the management of event. A possible solution would be to let the user decide if he prefer to use one global mutex for signals or one mutex per signal.
- Try to see if the usage of effects could lead to better performances for continuation handling.
- Create a good interface in the compiler to help the user write thread-safe code.

# Testing

You can do the command `make` in order to build the compiler.

Then multiple commands will be available to you :

- `make clean` to clean everything
- `make test` to launch the different tests
- `make darwin` to build and launch the executable `test/darwin.ml` from the related execise.
- `make compare` to build and launch the test on fibonacci `test/compare_perf.ml`

# My work

I have modified a few files from `ppx_rml` to do this project :

- `test/compare_perf.ml` to benchmark fibonacci
- `interpreter/lco_ctrl_tree.ml` to parallelise the interpreter
- `interpreter/sig_env.ml` to modify the signals interface to allow locks
- `interpreter/event_security.ml` to implement the two different types of mutexes associated to the events
- a few small things in the compiler to allow choosing the number of threads
